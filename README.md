# Causal Generation

Causal Generation is a promising activation function or linear replacement of the attention mechanism in decoders. The standard attention in large instruction decoder-only models collapses to attention sinks, which mainly [focus on one (start) token](https://colab.research.google.com/drive/1Fcgug4a6rv9F-Wej0rNveiM_SMNZOtrr?usp=sharing) with [massive activations](https://arxiv.org/abs/2402.17762). [Signals exchanged in the tail end of the embedding spectrum are responsible for attention sinking](https://arxiv.org/abs/2402.09221). Those maximas can be directly propagated by causaly calculating the extremum with the previous token.

This repository contains the code and model weights for my preprint paper ["Breaking the Attention Bottleneck"](https://arxiv.org/html/2406.10906v1) as an alternative to [calibrate the attention](https://github.com/GATECH-EIC/ACT) in the decoder.


## Mission Statement
My mission is to lower the barriers that hinder effective communication and knowledge dissemination in today's information-rich world. By leveraging the principles of the GNU affero general public license version 3 ("GNU AGPL v3 license"), I am committed to fostering a collaborative, open-access environment where research and insights are permanently freely available to the global community. Therefore, every derivate of my work has to stay freely distributed with the reproducible model source-code and trained weights.

I believe that the unrestricted sharing of information catalyzes innovation, nurtures collaboration, and promotes a humanistic approach to development. My goal is to empower researchers, educators, and innovators to combine and build upon existing knowledge, thereby accelerating advancements and generating impactful solutions that address pressing societal challenges.
The GNU AGPL v3 license ensures that my findings and resources are freely accessible to all, which reduces financial and legal barriers to information sharing.
This enables the appropriate development that prioritizes the well-being of individuals and communities to uphold the ethical standards and responsibility of humanistic values, which I can't ensure in all my endeavors on my own. Any contribution that acknowledges those values is welcome.

## Usage of the prototype

- Copy the model.py and checkpoint.pt into a clone of [nanoGPT](https://github.com/karpathy/nanoGPT)
- Run the inference `python3 sample.py --out_dir=out-shakespeare-char --device=cpu`

Have a look a the [vidoe tutorial](https://www.youtube.com/watch?v=kCc8FmEb1nY) if you are unfamiliar with nanoGPT. There are even more possible changes, e.g. [reweight-GPT](https://github.com/hunar4321/reweight-gpt).

## Generative static(x) function

One-directional decoder-only attention models, such as GPT are commonly employed for instruction-following tasks. Those instruction-tuned models have an informationless redundant attention pattern, which can be approximated with a static function. Let be "x" the batched input of tokenized sentences, then this function can be ironically be called [static(x)](https://www.youtube.com/@staticx/videos).

```
# A basic PyTorch example

def static(x):
  """
  This function is a simplification of the causal attention as a static reduction.
  The first token can only attend to itself, and remains unchanged.
  Every following token is minimized with the previous one.
  This comparison also works equally well with maximization.
  """
  # get the sequence length and dimension of the batch
  sequence_length, token_dimension = x.size()
    # return the batch if there is only one unchanged token
  if sequence_length == 1:
    return x
  # take the unchanged first token and unsqueeze it into a single tensor
  first_sequence = x[0].unsqueeze(0)
  # Concatenate the first sequence token with an empty batch.
  static_reduction = torch.concat((first_sequence, torch.zeros(sequence_length-1, token_dimension)),0)

  # reduce each token with the previous one
  for token_number in range(1, sequence_length):
    static_reduction[token_number] = torch.min(batch[token_number], batch[token_number-1])

  return static_reduction

```

The sequential for loop can be executed in parallel by writing the causal shift into one tensor:

```
  # Concatenate the first sequence token with an empty batch.
  causal_shift = torch.concat((first_sequence, x[:-1]),0)
  # stack of the original tensor with the causal shift and the global mean
  concatenation = torch.stack((x, causal_shift),1)
  # reduce the doubled tensor by the minimum
  static_reduction = torch.min(concatenation, dim=1)
```

The causal generation function can also weakly model a global context with an average mean vector:

```
# the causal context function calculated in one unity matrix

def causal_context_unity_matrix(batch):
  """
  This function is a simplification of the causal attention as a static reduction.
  The first token can only attend to itself and remains unchanged.
  Every following token is compared with the previous one.
  A global mean is additional calculated.
  """
  sequence_length, token_dimension = batch.size()

  # calculate the global mean
  global_mean = torch.mean(batch, dim=0)
  # take the unchanged first token and unsqueeze it into a single tensor
  first_sequence = batch[0].unsqueeze(0)
  # repeat the global mean into a tensor, the first token is copied
  global_tensor = torch.concat((first_sequence, global_mean.repeat(sequence_length-1,1)), 0)
  # concatenate the first sequence token with an empty batch
  causal_shift = torch.concat((first_sequence, batch[:-1]),0)
  # stack of the original tensor with the causal shift and the global mean
  concatenation = torch.stack((batch, causal_shift, global_tensor),1)
  static_reduction = torch.max(concatenation, dim=1)

  # only return the max values without the index
  return static_reduction[0]
```

Those functions can be used as replacement or addition to the standard attention:
```
    def forward(self, x):
        B, T, C = x.size() # batch size, sequence length, embedding dimensionality (n_embd)
        #z = self.input_c_proj(x)
        y = vmap(causal_context_unity_matrix)(x)
        # output projection
        y = self.resid_dropout(self.c_proj(y))
        return y
```
 Or it can be used as transformer activation, e.g. in the perceptron:
```
class causalContextMLP(nn.Module):
    """
    Add the causal context function as activation.
    This also works as replacement of the GELU activation at the upscaled inner dimension, though then it consumes more resources.
    """

    def __init__(self, config):
        super().__init__()
        self.c_fc    = nn.Linear(config.n_embd, int(config.n_embd*4), bias=config.bias)
        self.gelu    = nn.GELU()
        self.c_proj  = nn.Linear(int(config.n_embd*4), config.n_embd, bias=config.bias)
        self.dropout = nn.Dropout(config.dropout)

    def forward(self, x):
        x = vmap(causal_context_unity_matrix)(x)
        x = self.c_fc(x)
        x = self.gelu(x)
        x = self.c_proj(x)
        return x

class causalMLP(nn.Module):
    """
    Add the causal function as activation.
    A combination with the causalContextMLP yields slightly better results.
    """

    def __init__(self, config):
        super().__init__()
        self.c_fc    = nn.Linear(config.n_embd, int(config.n_embd*4), bias=config.bias)
        self.gelu    = nn.GELU()
        self.c_proj  = nn.Linear(int(config.n_embd*4), config.n_embd, bias=config.bias)
        self.dropout = nn.Dropout(config.dropout)

    def forward(self, x):
        
        x = self.c_fc(x)
        x = self.gelu(x)
        x = self.c_proj(x)
        return x
```

## Future Work
- [ ] Integrate my concept into the huggingface ecosystem
- [ ] Upscale this concept and test various settings
- [ ] Test [torch.compile(causal_generation)](https://pytorch.org/blog/Accelerating-Hugging-Face-and-TIMM-models/) as optimization

## GNU AGPL license
This work is distributed under the GNU AGPL v3 license. Any derivate has to fulfil the same standard of reproducible source-coude and shared model weights.

## Citation
If you use this concept, the trained weights or the code, please cite:

    @misc{hilsenbek2024breaking,
      title={Breaking the Attention Bottleneck}, 
      author={Kalle Hilsenbek},
      year={2024},
      eprint={2406.10906},
      archivePrefix={arXiv}
}


![transformers_breaking_bottleneck](dual_transformer.png)