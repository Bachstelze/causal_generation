# distributed under the GNU AGPL v3 license 
import torch

def causal_context_max(batch):
    """
    vmap can't call a function with many parameters,
    so this function routes it to the algorithm.
    """
    return causal_context(batch, "max")

def causal_context_min(batch):
    """
    vmap can't call a function with many parameters,
    so this function routes it to the algorithm.
    """
    return causal_context(batch, "min")

def causal_context(batch, function_type):
  """
  This function is a simplification of the causal attention as a static reduction.
  The first token can only attend to itself and remains unchanged.
  Every following token is compared with the previous one.
  A global mean is calculated additional.
  """
  sequence_length, token_dimension = batch.size()
  # calculate the global mean
  global_mean = torch.mean(batch, dim=0)
  # take the unchanged first token and unsqueeze it into a single tensor
  first_sequence = batch[0]
  # the first token could be also be compared with the global mean:
  # first_sequence = torch.max(batch[0], global_mean)
  # though this leads to overfitting with a slightly lower training loss and a bit higher validation loss

  # Concatenate the first sequence token with an empty batch
  static_reduction = torch.concat((first_sequence.unsqueeze(0), torch.zeros(sequence_length-1, token_dimension)),0)

  # reduce each token with the previous one
  for token_number in range(1, sequence_length):
    if function_type == "min":
        couple = torch.min(batch[token_number], batch[token_number-1])
        static_reduction[token_number] = torch.min(couple, global_mean)
    else: # function_type == "max":
        couple = torch.max(batch[token_number], batch[token_number-1])
        static_reduction[token_number] = torch.max(couple, global_mean)
  return static_reduction

def causal_context_unity_matrix(batch):
  """
  This function is a simplification of the causal attention as a static reduction.
  The first token can only attend to itself and remains unchanged.
  Every following token is compared with the previous one.
  A global mean is calculated additional.
  """
  sequence_length, token_dimension = batch.size()

  # calculate the global mean
  global_mean = torch.mean(batch, dim=0)
  # take the unchanged first token and unqsueeze it into a single tensor
  first_sequence = batch[0].unsqueeze(0)
  # repeat the global mean into a tensor, the first token is copied
  global_tensor = torch.concat((first_sequence, global_mean.repeat(sequence_length-1,1)), 0)
  # Concatenate the first sequence token with an empty batch
  causal_shift = torch.concat((first_sequence, batch[:-1]),0)
  # stack of the original tensor with the causal shift and the global mean
  concatenation = torch.stack((batch, causal_shift, global_tensor),1)
  static_reduction = torch.max(concatenation, dim=1)

  # only return the max values without the index
  return static_reduction[0]
